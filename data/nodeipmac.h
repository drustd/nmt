#ifndef NODEIPMAC_H
#define NODEIPMAC_H

#include "node.h"
#include <QMap>

class NodeIpMac : public Node
{
public:
    NodeIpMac();
    QMap<QString, QString> *ipMacTable;

};

#endif // NODEIPMAC_H
