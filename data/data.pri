HEADERS += \
    $$PWD/host.h \
    $$PWD/node.h \
    $$PWD/nodeipmac.h \
    $$PWD/router.h \
    $$PWD/snmpswitch.h

SOURCES += \
    $$PWD/host.cpp \
    $$PWD/node.cpp \
    $$PWD/nodeipmac.cpp \
    $$PWD/router.cpp \
    $$PWD/snmpswitch.cpp
