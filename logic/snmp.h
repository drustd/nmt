#ifndef SNMP_H
#define SNMP_H

#include <snmp_pp/snmp_pp.h>
#include <snmp_pp/target.h>

#include "snmpconfiguration.h"

class SNMP
{
public:
    SNMP(QString);
    void get_sytem_descryptor();
    void get_FdbTable();

    struct SnmpResponse {
        Oid oid;

    };

private:
    SnmpConfiguration *cnfg;
    CTarget target;
};

#endif // SNMP_H
