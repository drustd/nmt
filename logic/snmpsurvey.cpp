#include "snmpsurvey.h"
#include "snmp.h"

SnmpSurvey::SnmpSurvey(QList<QString> &aliveIp)
{
    this->ipRange = aliveIp;
}

void SnmpSurvey::checkAliveIpBySnmp()
{
    SNMP *snmp;

    foreach (QString ip, ipRange) {
        snmp = new SNMP(ip);
        snmp->get_sytem_descryptor();
        snmp->get_FdbTable();
    }
}
