#include "snmpconfiguration.h"

SnmpConfiguration::SnmpConfiguration(QString ipHost)
{
    QByteArray ba = ipHost.toLatin1();
    const char *in = ba.data();
    hostAddress = new UdpAddress(in);
}

UdpAddress SnmpConfiguration::getHostAddress() const
{
    return *hostAddress;
}
