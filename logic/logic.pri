HEADERS += \
    $$PWD/netscan.h \
    $$PWD/ping.h \
    $$PWD/snmp.h \
    $$PWD/snmpconfiguration.h \
    $$PWD/snmpsurvey.h

SOURCES += \
    $$PWD/netscan.cpp \
    $$PWD/ping.cpp \
    $$PWD/snmp.cpp \
    $$PWD/snmpconfiguration.cpp \
    $$PWD/snmpsurvey.cpp
