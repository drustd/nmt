#ifndef SNMPSURVEY_H
#define SNMPSURVEY_H

#include <QList>
#include <QString>

class SnmpSurvey
{
public:
    SnmpSurvey(QList<QString> &aliveIp);
    void checkAliveIpBySnmp();

private:
    QList<QString> ipRange;
};

#endif // SNMPSURVEY_H
