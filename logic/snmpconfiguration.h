#ifndef SNMPCONFIGURATION_H
#define SNMPCONFIGURATION_H

#include <QString>

#include <snmp_pp/snmp_pp.h>

using namespace Snmp_pp;

class SnmpConfiguration
{
public:
    SnmpConfiguration(QString ipHost);
    UdpAddress getHostAddress() const;

private:
    UdpAddress *hostAddress;
    snmp_version version;
    QString community;
    QString username;
    QString password;
    qint16 port;
    qint32 timeout;
};

#endif // SNMPCONFIGURATION_H
