#ifndef NETSCAN_H
#define NETSCAN_H

#include <QList>
#include <QThreadPool>

class NetScan
{
public:
    NetScan();
    QList<QString> scanIpRange();

private:
    QList<QString> ipRange;
};

#endif // NETSCAN_H
