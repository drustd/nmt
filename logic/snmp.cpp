#include "snmp.h"
#include <QDebug>

using namespace  Snmp_pp;
#define SYSDESCR        "1.3.6.1.2.1.1.1.0"              // Object ID for System Descriptor
#define FDB_TABLE       "1.3.6.1.2.1.17.4.3"
#define BASE_PORT_TABLE "1.3.6.1.2.1.17.1.4"
#define IF_TABLE        "1.3.6.1.2.1.2.2"
#define IP_ROUTE_TABLE  "1.3.6.1.2.1.4.21"

SNMP::SNMP(QString ip)
{
    cnfg = new SnmpConfiguration("127.0.0.1:1024");     // ... temporariely for all ip`s
}

/*
 * The actual SNMP++ calls are made up of ten lines of code. A CTarget object is
 * created using the IP address of the agent. A variable binding (Vb) object is
 * then created using the object identifier of the MIB object to retrieve
 * (System Descriptor). The Vb object is then attached to a Pdu object. An Snmp
 * object is used to invoke an SNMP get. Once retrieved, the response message is
 * printed out. All error handling code is included.
 * */
void SNMP::get_sytem_descryptor(){
    int status;

    //CTarget contain timeout, retransmission, protocol type, community,
    //etc. configuration
    target.set_address(cnfg->getHostAddress());
    target.set_version(version2c);

    Vb vb(SYSDESCR);
    Pdu pdu;

    //create a snmp++ session and check status
    Snmp snmp(status);
    if (status != SNMP_CLASS_SUCCESS) {
        qDebug(snmp.error_msg(status));
        return;
    }

    pdu += vb;

    if ( (status = snmp.get(pdu, target)) != SNMP_CLASS_SUCCESS)
        qDebug(snmp.error_msg(status));
    else {
        pdu.get_vb(vb, 0);
        qDebug() << "System Descriptor =" << vb.get_printable_value();
    }
}

void SNMP::get_FdbTable() {
    int status;

    target.set_address(cnfg->getHostAddress());
    Vb vb(FDB_TABLE);
    Pdu pdu;

    Snmp snmp(status);
    pdu += vb;

    QString responseOid;
    QString responseValue;

    QMap<QString, QString> oidValueAccord;
    QMap<QString, QString> fdbTable;

    if ((status = snmp.get_bulk(pdu, target, 0, 10)) != SNMP_CLASS_SUCCESS)
        qDebug(snmp.error_msg(status));
    else  {
        for (int i = 0; i < pdu.get_vb_count(); i++ ){
            pdu.get_vb(vb, i);
            //qDebug() << "Oid = " << vb.get_printable_oid();
            responseOid = vb.get_printable_oid();

            if ( vb.get_syntax() != sNMP_SYNTAX_ENDOFMIBVIEW){
                //qDebug() << "VAlue = " << vb.get_printable_value();
                responseValue = vb.get_printable_value();

//                OctetStr value;
//                vb.get_value(value);
//                QString rawValue(value.get_printable_hex());
//                qDebug() << rawValue;
            }
            else {
                qDebug() << "End of MIB";
            }

            oidValueAccord.insert(responseOid, responseValue);
            responseOid.clear();
            responseValue.clear();
        }
    }

    QStringList macAddress;
    QStringList port;

    foreach (QString oid, oidValueAccord.keys()) {

        if (oid.indexOf("1.3.6.1.2.1.17.4.3.1.1", 0) != -1){
            // parse mac addres into string
            QString strToParse = oidValueAccord.value(oid);
            //QRegExp macExp("([0-9a-f]{2}[:-\s]){5}([0-9a-f]{2})");
            QRegExp rx("[0-9A-F]{2}");
            QString simpleStr = strToParse.simplified();

            QStringList list;
            int pos = 0;

            while ((pos = rx.indexIn(simpleStr, pos)) != -1) {
                list << rx.cap(0);
                pos += rx.matchedLength();
            }

            QString macString = list.join(':');
            macAddress.append(macString);
        }

        if (oid.indexOf("1.3.6.1.2.1.17.4.3.1.2", 0) != -1){
            port.append(oidValueAccord.value(oid));
        }
    }

    if (macAddress.count() != port.count())
        qDebug() << "Error count";

    for ( int i = 0; i < macAddress.count(); i++){
        fdbTable.insert(macAddress[i], port[i]);
    }

    QMap<QString, QString>::iterator it = fdbTable.begin();
    for( ; it != fdbTable.end(); ++it){
        qDebug() << "Mac : " << it.key() << "Port : " << it.value();
    }
}


