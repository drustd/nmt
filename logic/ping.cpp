#include "ping.h"

Ping::Ping(QString ipAddress, QList<QString> *aliveIpList, QMutex *mutex)
{
#if defined(WIN32)
    parameters << "-n" << "1";
#else
    parameters << "-c 1 -q";
#endif
    parameters << ipAddress;

    this->aliveIpList = aliveIpList;
    this->mutex = mutex;
}

void Ping::run()
{
    int exitCode = QProcess::execute("ping", parameters);
    if (exitCode==0) {
        // it's alive
        qDebug("it's alive");
        QMutexLocker locker(mutex);
        aliveIpList->append(parameters.last());
    } else {
        // it's dead
        qDebug("it's dead");
    }
}
