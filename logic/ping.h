#ifndef PING_H
#define PING_H

#include <QString>
#include <QProcess>
#include <QRunnable>
#include <QMutex>

class Ping : public QRunnable
{
public:
    Ping(QString, QList<QString>*, QMutex*);

    // QRunnable interface
    void run();

private:
    mutable QMutex *mutex;
    QStringList parameters;
    QList<QString> *aliveIpList;
};

#endif // PING_H
