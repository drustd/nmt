#include "netscan.h"
#include "ping.h"
#include <QDebug>

NetScan::NetScan()
{
    QString network("91.109.200.");           //data center
    //QString network("191.168.1.");

    for (int i = 0; i <= 25; i++){
        QString host = QString("%1%2")
                .arg(network).arg(i);
        ipRange.append(host);
    }
}

QList<QString> NetScan::scanIpRange(){
    QMutex mutex;
    QList<QString> aliveIp;
    QThreadPool::globalInstance()->setMaxThreadCount(25);
    Ping *ping;

    foreach (QString var, ipRange) {
        ping = new Ping(var, &aliveIp, &mutex);
        ping->setAutoDelete(true);
        QThreadPool::globalInstance()->start(ping);
    }

    QThreadPool::globalInstance()->waitForDone(-1);
    qDebug("scan complete");

    foreach (QString var, aliveIp) {
        qDebug() << var;
    }

    return aliveIp;

}
