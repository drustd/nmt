#include <QCoreApplication>
#include "logic/snmp.h"
#include "logic/ping.h"
#include "logic/netscan.h"
#include "logic/snmpsurvey.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//    SNMP snmp("11");
//    snmp.get_sytem_descryptor();
//    snmp.get_FdbTable();

    //Ping ping("192.168.1.1");

    NetScan scan;
    QList<QString> aliveIp = scan.scanIpRange();

    SnmpSurvey snmpRequest(aliveIp);
    snmpRequest.checkAliveIpBySnmp();


    return a.exec();
}
